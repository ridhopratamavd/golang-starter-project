package main

import (
	"fmt"
	"tahu.com/user/hello/morestrings/source"
)

func main() {
	fmt.Println(source.ReverseRunes("nayamul heb"))   // niku
	fmt.Println(source.SwapString("jonathan", "cul")) // niku
	fmt.Println(source.NumberConcatString(4, "cul"))  // niku
}
