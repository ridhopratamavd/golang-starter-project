package source

import (
	"bytes"
	"strconv"
)

// ReverseRunes returns its argument string reversed rune-wise left to right.
func NumberConcatString(n uint32, s string) string {
	var b bytes.Buffer

	b.WriteString(strconv.FormatInt(int64(n), 10))
	b.WriteString(" ")
	b.WriteString(s)

	return b.String()
}
