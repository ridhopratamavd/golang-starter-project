package source

import "bytes"

func SwapString(x, y string) string {
	var b bytes.Buffer

	b.WriteString(y)
	b.WriteString(" ")
	b.WriteString(x)

	return b.String()
}
