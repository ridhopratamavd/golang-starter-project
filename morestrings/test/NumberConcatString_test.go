package test

import (
	"tahu.com/user/hello/morestrings/source"
	"testing"
)

func TestNumberConcatString(t *testing.T) {
	type args struct {
		n uint32
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"names", args{4, "ola"}, "4 ola"},
		{"namex", args{4, "oli"}, "4 oli"},
		{"namej", args{4, "ole"}, "4 ole"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := source.NumberConcatString(tt.args.n, tt.args.s); got != tt.want {
				t.Errorf("NumberConcatString() = %v, want %v", got, tt.want)
			}
		})
	}
}
