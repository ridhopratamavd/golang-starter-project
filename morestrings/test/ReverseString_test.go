package test

import (
	"tahu.com/user/hello/morestrings/source"
	"testing"
)

func TestReverseRunes(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		description string
		args        args
		want        string
	}{
		{"it should lala", args{"Hello, world"}, "dlrow ,olleH"},
		{"it should lili", args{"ulul, world"}, "dlrow ,lulu"},
	}
	for _, tt := range tests {
		t.Run(tt.description, func(t *testing.T) {
			if got := source.ReverseRunes(tt.args.s); got != tt.want {
				t.Errorf("ReverseRunes() = %v, want %v", got, tt.want)
			}
		})
	}
}
