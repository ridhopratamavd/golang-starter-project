package test

import (
	"tahu.com/user/hello/morestrings/source"
	"testing"
)

func TestSwapString(t *testing.T) {
	type args struct {
		x string
		y string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"it should loolo", args{"Hello", "World"}, "World Hello"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := source.SwapString(tt.args.x, tt.args.y); got != tt.want {
				t.Errorf("SwapString() = %v, want %v", got, tt.want)
			}
		})
	}
}
