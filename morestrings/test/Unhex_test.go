package test

import (
	"tahu.com/user/hello/morestrings/source"
	"testing"
)

func TestUnhex(t *testing.T) {
	type args struct {
		c byte
	}
	tests := []struct {
		name string
		args args
		want byte
	}{
		{"it should ", args{98}, 11},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := source.Unhex(tt.args.c); got != tt.want {
				t.Errorf("Unhex() = %v, want %v", got, tt.want)
			}
		})
	}
}
